package c.a.shp.tfront.configuration;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * Klasa do skopiowania dla nowych typów błędów. Przekierowuje nas na własne mappingi.
 */
@Configuration
public class ErrorConfig implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    @Override
    public void customize(ConfigurableServletWebServerFactory factory) {
        factory.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/error/404"));
        factory.addErrorPages(new ErrorPage(HttpStatus.BAD_REQUEST, "/error/error"),
                new ErrorPage(HttpStatus.NO_CONTENT, "/error/error"),
                new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/error"));
    }
}
