package c.a.shp.tfront.controller;

import c.a.shp.tfront.model.AppUser;
import c.a.shp.tfront.model.ResetPasswordRequest;
import c.a.shp.tfront.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/")
public class IndexController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/")
    public String index() {
        return "index";
    }

    @GetMapping("/login")
    public String loginForm() {
        return "loginForm";
    }

    @GetMapping("/register")
    public String registerForm(Model model) {
        model.addAttribute("user", new AppUser());
        return "user/userRegisterForm";
    }

    @PostMapping("/register")
    public String registerUser(AppUser appUser, String passwordConfirm) {
        userService.registerUser(appUser, passwordConfirm);
        return "redirect:/";
    }

    @GetMapping("/activate/{activation_id}")
    public String activate(@PathVariable("activation_id") String activationId,
                           Model model) {
        model.addAttribute("success", userService.activate(activationId));
        return "user/activationResult";
    }

    // trzeba stworzyć mappingi na reset.
    // Pierwsze dwa to formularz gdzie wpisujemy maila i wtedy na nasze konto wysyłany jest link aktywacyjny.
    @GetMapping("/reset")
    public String getResetSubmitionForm() {
        return "resetSubmitionForm";
    }

    @PostMapping("/resetSubmit")
    public String resetSubmition(Model model,
                                 @RequestParam(name = "resetEmail") String email) {
        model.addAttribute("success", userService.submitResetPasswordRequest(email)); // jesli bedzie sukces to metoda zwroci true
        return "resetSubmitionResult";
    }

    @GetMapping("/passwordReset/{reset_id}")
    public String getResetForm(Model model,
                               @PathVariable("reset_id") String resetId) {
        model.addAttribute("reset_id", resetId);
        return "resetPasswordForm";
    }

    @PostMapping("/passwordReset/{reset_id}")
    public String getResetForm(Model model,
                               @PathVariable("reset_id") String resetId,
                               ResetPasswordRequest resetPasswordRequest) {
        if (resetPasswordRequest.getPassword().equals(resetPasswordRequest.getPasswordConfirm())) {
            model.addAttribute("success", userService.resetPassword(resetId, resetPasswordRequest)); // jesli bedzie sukces to metoda zwroci true
            return "resetSubmitionResult";
        }
        return "redirect:/passwordReset/" + resetId;
    }

}
