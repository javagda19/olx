package c.a.shp.tfront.service;

import c.a.shp.tfront.model.AppUser;
import c.a.shp.tfront.model.UserRole;
import c.a.shp.tfront.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<AppUser> appUserOptional = appUserRepository.findByEmail(email);
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();

            String[] rolesArray = appUser.getRoles()
                    .stream()
                    .map(UserRole::getName)
                    .toArray(String[]::new);

            return User.builder()
                    .username(appUser.getEmail())
                    .password(appUser.getPassword())
                    .roles(rolesArray)
                    .disabled(appUser.getActivationCode() != null)
                    .build();
        }

        throw new UsernameNotFoundException("Unable to find user with that email.");
    }
}
