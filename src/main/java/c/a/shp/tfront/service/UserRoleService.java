package c.a.shp.tfront.service;

import c.a.shp.tfront.model.UserRole;

import java.util.Set;

public interface UserRoleService {
    Set<UserRole> getDefaultUserRoles();
}
