package c.a.shp.tfront.service;

import c.a.shp.tfront.model.AppUser;
import c.a.shp.tfront.model.ResetPasswordRequest;

import java.util.List;

public interface UserService {
//    void registerUser(String username, String password, String passwordConfirm);

    List<AppUser> getAllUsers();

    void registerUser(AppUser appUserm, String passwordConfirm);

    boolean activate(String activationId);

    boolean submitResetPasswordRequest(String email);

    boolean resetPassword(String resetId, ResetPasswordRequest password);
}
