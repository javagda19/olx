package c.a.shp.tfront.repository;

import c.a.shp.tfront.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByEmail(String email);

    boolean existsByEmail(String email);

    Optional<AppUser> findByActivationCode(String activationId);

    Optional<AppUser> findByResetId(String resetId);
}
