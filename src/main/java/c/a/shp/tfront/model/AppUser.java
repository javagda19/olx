package c.a.shp.tfront.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String email;
    private String password;

    private String firstName;
    private String lastName;

    @Column(nullable = true)
    private String activationCode;

    @Column(nullable = true)
    private String resetId;

    @ManyToMany()
    private Set<UserRole> roles;
}
