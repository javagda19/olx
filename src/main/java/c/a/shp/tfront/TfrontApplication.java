package c.a.shp.tfront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TfrontApplication {

    public static void main(String[] args) {
        SpringApplication.run(TfrontApplication.class, args);
    }

}
